# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 12:25:40 2022

@author: jocon

Intro to Mechatronics Lab 0 Fibonacci

"""

def fib(x):                                 # fibonacci generator
    
    n0 = 0                                  # defining first two fibo numbers
    n1 = 1
    i = 0                                   # counting variable
    
    if x == 0:                              # defining the index 0 value
        
        n = n0
       
    elif x == 1:                            # defining the index 1 value
        
        n = n1
        
    else:                                   # for all other indexes
        
        while i < x - 1:
            
            n = n0 + n1
            n0 = n1
            n1 = n
           
            i = i + 1
            
    return n

if __name__ == '__main__':
  
    idx = (input("Input and index value: "))     # asking user for index value
    
    numcheck = str(idx.isdigit())
    
    while numcheck == "False":                          # making sure user input is an integer 
        
        print("Index needs to be an integer")
        
        idx = (input("Input and index value: "))     # asking user for index value
    
        numcheck = str(idx.isdigit())
    
    
    fibo = fib(int(idx))
    
    print(f"The {idx}th is {fibo}")                 # printing out user specified fibonacci number
    
    next = str()
    
    while next != "quit":                           # asking if user wants to continue use            
        
        next = input("If you are done using this program type 'quit' if you want to continue type 'cont'")
        
        if next == "cont":
            
            idx = (input("Input and index value: "))     # asking user for index value
    
            numcheck = str(idx.isdigit())
    
            while numcheck == "False":                          # making sure user input is an integer 
                
                print("Index needs to be an integer")
                
                idx = (input("Input and index value: "))     # asking user for index value
    
                numcheck = str(idx.isdigit())
    
    
            fibo = fib(int(idx))
    
            print(f"The {idx}th is {fibo}")                 # printing out user specified fibonacci number
           

    

    

    
    
    
