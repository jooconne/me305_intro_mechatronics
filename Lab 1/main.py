'''! @file main.py
    @brief Change the brightness of an LED in different waveforms, changes after button press
    @details After button press, the code changes between three wave forms; Square, Sine, and Saw
    @image html L1_TaskDiagram.jpg "Task Diagram" width=500px
    @image html L1_StateDiagram.jpg "State Transition Diagram" width=500px
    @author OConnell Joseph
    @author Osborne Barrett
    @date 01/13/2022
    
'''

import time
import pyb
import math

def onButtonPressFCN(IRQ_src):
    
    '''! 
        @brief Shows that the button has been pressed
        
        @details Sees that the button has been pressed and then changes the value of a variable to true
        
        @return This returns that the button is pressed
    '''
    global ButtonIsPressed
    ButtonIsPressed = True
    
def SquareWave(TimeSinceStart):
    
    '''! 
        @brief Outputs a squarewave
    
        @details outputs a brightness value which takes the form of a squarewave
        
        @param TimeSinceStart a time value that will be plugged into the squarewave function
        
        @return brightness value the varies as a squarewave
    
    '''
    
    ##  @brief      brightness value for LED
    #   @details    brightness with change as a function of time
    #               the brightness function is different for each state
    #
    #brightness = round(math.remainder(TimeSinceStart/100, 2))
    brightness = round(TimeSinceStart/500)%2 * 100 #if math lib doesnt work bc it was acting funky today
    return brightness

def SawWave(TimeSinceStart):
    
    '''! 
        @brief Outputs a sawwave
    
        @details outputs a brightness value which takes the form of a sawwave
        
        @param TimeSinceStart a time value that will be plugged into the sawwave function
        
        @return brightness value the varies as a sawwave
    
    '''
    #brightness = round(math.remainder(TimeSinceStart,1000))/10
    brightness = round(TimeSinceStart%1000)/10  #if math lib doesnt work bc it was acting funky today
    return brightness

def SineWave(TimeSinceStart):
    
    '''! Outputs a sinwave
    
        @details outputs a brightness value which takes the form of a sinwave
        
        @param TimeSinceStart a time value that will be plugged into the sinwave function
        
        @return brightness value the varies as a sinwave
    
    '''
    brightness = 50*math.sin(TimeSinceStart * 0.001 * math.pi / 5)+50
    return brightness


if __name__ == '__main__':
    
    print("Welcome! Press the blue button to change how the LED blinks! Press ctrl + c to stop running")
    
    ##  @brief Keeps track of the state of the system
    #
    state = 0
    
    ##  @brief Keeps track of the button
    #
    ButtonIsPressed = False
    
    # Code associated with LED
    pinLED = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2 , freq = 20000)
    t2ch1 = tim2.channel(1 , pyb.Timer.PWM , pin = pinLED)
    
    # Code associated with button
    pinBut = pyb.Pin (pyb.Pin.cpu.C13)
    ButtonInt = pyb.ExtInt(pinBut , mode = pyb.ExtInt.IRQ_FALLING , pull = pyb.Pin.PULL_NONE , callback = onButtonPressFCN)
    
    ##  @brief Gives the start time of the program
    #
    StartTime = time.ticks_ms()
    
    while True:
    
        try:
            
            if state == 0:  # Off State
                ##  @brief current time minus start time
                #
                WaveTime = time.ticks_diff(time.ticks_ms() , StartTime)
                brightness = 0
                t2ch1.pulse_width_percent(brightness)
                
                if ButtonIsPressed:
                    StartTime = time.ticks_ms()
                    ButtonIsPressed = False
                    state = 1
                    print("Square")
            
            elif state == 1:  # Square State
                WaveTime = time.ticks_diff(time.ticks_ms() , StartTime)
                brightness = SquareWave(WaveTime)
                t2ch1.pulse_width_percent(brightness)
                
                
                if ButtonIsPressed:
                    StartTime = time.ticks_ms()
                    ButtonIsPressed = False
                    state = 2
                    print("Sine")
          
            
            elif state == 2:  # Sine State
                WaveTime = time.ticks_diff(time.ticks_ms() , StartTime)
                brightness = SineWave(WaveTime)
                t2ch1.pulse_width_percent(brightness)
                
                if ButtonIsPressed:
                    StartTime = time.ticks_ms()
                    ButtonIsPressed = False
                    state = 3
                    print("Saw")
            
            elif state == 3:  # Saw State
                WaveTime = time.ticks_diff(time.ticks_ms() , StartTime)
                brightness = SawWave(WaveTime)
                t2ch1.pulse_width_percent(brightness)
                
                if ButtonIsPressed:
                    StartTime = time.ticks_ms()
                    ButtonIsPressed = False
                    state = 1
                    print("Square")
        
        except KeyboardInterrupt:
            break
    
    print("Program Terminating")
        
        
        

